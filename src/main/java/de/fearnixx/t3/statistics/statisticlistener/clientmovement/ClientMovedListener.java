package de.fearnixx.t3.statistics.statisticlistener.clientmovement;

import de.fearnixx.jeak.event.IQueryEvent;
import de.fearnixx.jeak.reflect.Listener;
import de.fearnixx.t3.statistics.LogEntry;
import de.fearnixx.t3.statistics.TS3Statistics;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.Optional;


public class ClientMovedListener {

    private EntityManager entityManager;
    private final TS3Statistics statisticsPlugin;

    public ClientMovedListener(EntityManager entityManager, TS3Statistics statisticsPlugin) {
        this.entityManager = entityManager;
        this.statisticsPlugin = statisticsPlugin;
    }

    @Listener(order = Listener.Orders.NORMAL)
    public boolean onEvent(IQueryEvent.INotification.IClientMoved event) {
        Optional<String> eventLogEntryID = event.getProperty(TS3Statistics.TS3_STATISTICS_LOG_ENTRY_ID);
        if (!eventLogEntryID.isPresent()) {
            return false;
        }

        ClientMovement clientMovement = new ClientMovement();

        Optional<String> reason = event.getProperty("reasonid");
        Optional<String> channelId = event.getProperty("ctid");

        if (reason.isPresent() && channelId.isPresent()) {
            clientMovement.setReason(Integer.valueOf(reason.get()));
            clientMovement.setChannelId(Integer.valueOf(channelId.get()));
        } else {
            throw new RuntimeException("At least one expected value 'ctid' or 'reasonid' was not found!");
        }

        TypedQuery<LogEntry> query = entityManager
                .createQuery("SELECT t FROM LogEntry t WHERE t.entryID = :id", LogEntry.class);
        query.setParameter("id", Long.valueOf(eventLogEntryID.get()));

        synchronized (statisticsPlugin) {

            final EntityTransaction transaction = entityManager.getTransaction();

            try {
                LogEntry logEntry = query.getSingleResult();
                clientMovement.setLogEntry(logEntry);
            } catch (NoResultException e) {
                throw new IllegalStateException(
                        "No LogEntry with the specified Entry-ID " +
                                "found although it should have been added!"
                );
            }

            try {
                transaction.begin();
                entityManager.persist(clientMovement);
                entityManager.flush();
                transaction.commit();
            } catch (Exception e) {
                entityManager.getTransaction().rollback();
            }
        }

        return true;
    }
}
