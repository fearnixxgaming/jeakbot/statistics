package de.fearnixx.t3.statistics;

import javax.persistence.*;

import java.io.Serializable;

@Entity(name = "EventType")
@Table(name = "EVENT_TYPES")
@SuppressWarnings("unused")
public class EventType implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TYPE_ID")
    private Integer typeId;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "TS3_EVENT")
    private String ts3Event;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getTs3Event() {
        return ts3Event;
    }

    public void setTs3Event(String ts3Event) {
        this.ts3Event = ts3Event;
    }
}
